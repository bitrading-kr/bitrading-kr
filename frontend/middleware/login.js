import qs from 'querystring'
import url from 'url'

const ERR = {
  ACCESS: { message: '잘못된 접근입니다.', code: 'ACCESS' },
  LOGIN: { message: '로그인에 실패하였습니다.', code: 'LOGIN' },
  ERROR: { message: '오류가 발생하였습니다.', code: 'ERROR' }
};
function NewError (code) {
  let data = ERR[code];
  let err = new Error(data.message);
  err.code = data.code;
  return err;
}
function ErrorHandler (error, defaultCode='ERROR') {
  let errCode = error.code;
  if (ERR[errCode] != null) return error;
  return NewError(defaultCode);
}

export default async (c) => {
  try {
    // console.log(c);
    console.log(`---------------`);
    // const { headers } = req,
    //       { BACKEND_API } = env;
    // let { referer, formdata } = headers;

    // // 헤더 유효성 검사
    // try {
    //   if (url.parse(referer).pathname != "/auth") throw new Error();
    //   if (typeof formdata != 'string') throw new Error();
    //   formdata = qs.parse(formdata);
    //   if (formdata.id == null || formdata.pw == null) throw new Error();
    // } catch (e) {
    //   throw NewError('ACCESS');
    // }

    // // 로그인 데이터 전송
    // try {
    //   // let res = await $backend.login(formdata.id, formdata.pw);
    //   let res = await $backend.login('hihi29', 'hihi1');
    //   store.state.token = res.data;
    //   console.log(store.state.token);
    //   return res.data;
    // } catch (e) {
    //   throw NewError('LOGIN');
    // }

  } catch (e) {
    // console.log(e);
    // throw ErrorHandler(e);
  }
}

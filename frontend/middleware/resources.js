const axios = require('axios'),
      url = require('url'),
      _ = require('lodash');

async function getExchanges (apiHost) {
  return await axios.get(url.resolve(apiHost, 'resources/exchanges'));
}
async function getMarkets (apiHost, exchange) {
  return await axios.get(url.resolve(apiHost, `resources/exchanges/${exchange}/markets`));
}
async function getCoins (apiHost, exchange) {
  return await axios.get(url.resolve(apiHost, `resources/exchanges/${exchange}/coins`));
}

export default ({ env, store, route, redirect}) => {
  // let exchanges = env.SUPPORT_EXCHANGES,
  //     host = env.BACKEND_API;
  // console.log(host);
  // if (store.state.Resources.req == 0) {
  //   store.commit('incReq');
  //   if (_.size(store.state.Resources.exchange) == 0) {
  //     getExchanges(host)
  //       .then(result => {
  //         let exchanges = result.data;
  //         store.commit('setExchange', exchanges);
  //         store.commit('resetReq');
  //         exchanges.forEach(({symbol}) => {
  //           getMarkets(host, symbol)
  //             .then(_res => store.commit('setMarket', [symbol, _res.data]))
  //             .catch(_err => { throw _err; });
  //           getCoins(host, symbol)
  //             .then(_res => store.commit('setCoin', [symbol, _res.data]))
  //             .catch(_err => { throw _err; });
  //         });
  //       })
  //       .catch(err => { throw err; });
  //   }
  // }
  // console.log(store.state.Resources.req);
}

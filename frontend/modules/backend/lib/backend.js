import axios from 'axios'
import url from 'url'
import _ from 'lodash'

class Backend {
  constructor (host) {
    this.host = host;
  }

  async post (_url, data) {
    return await axios.post(url.resolve(this.host, _url), data);
  }

  async get (_url) {
    return await axios.get(url.resolve(this.host, _url));
  }

  async login (id, password) {
    let result = await this.post('/api/auth/login', {
      id: id,
      password: password
    });
    return result;
  }

  set host (host) {
    this._host = host;
  }
  get host () {
    if (this._host == null)
      throw new Error('The host is not initialized.');
    return this._host;
  }
}

export default new Backend();

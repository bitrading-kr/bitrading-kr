import _ from 'lodash'
import url from 'url'
import cookie from 'cookie'

let middlewareUrl = `${process.env.MIDDLEWARE_PROTOCOL}://${process.env.MIDDLEWARE_HOST}:${process.env.MIDDLEWARE_PORT}${process.env.MIDDLEWARE_ROOT}`;

export default async ({ req, redirect, $backend, $axios, $errors }) => {
  // TODO: 로그인시 에러 X
  const cookieStr = _.get(req, 'headers.cookie');
  if (!_.isString(cookieStr)) {
    $errors.login("사용자 인증이 필요한 서비스입니다.");
  }

  const cookieData = cookie.parse(cookieStr);
  const token = _.get(cookieData, 'authtoken');
  if (!_.isString(token)) {
    $errors.login("사용자 인증이 필요한 서비스입니다.");
  }

  try {
    let middleware = `process.env.MIDDLEWARE`;
    let res = await $axios.get(url.resolve(middlewareUrl, '/api/auth/login'), {
      headers: {
        'auth-token': token
      }
    });
    if (res.data !== true) {
      $errors.login("사용자 인증이 필요한 서비스입니다.");
    }
  } catch (e) {
    console.log(e);
    $errors.login("사용자 인증이 필요한 서비스입니다.");
  }
}

"use strict";

class Resources {
  constructor (btrConnection) {
    this.btr = btrConnection;
  }

  // GET: /resources
  get () {
  }

  // POST: /resources
  post () {
  }

  // DELETE: /resources
  delete () {
  }

  // GET: /resources/exchanges
  async get_exchanges () {
    return await this.btr.Exchange.getAll();
  }

  // GET: /resources/exchanges/:symbol
  async get_exchange (symbol) {
  }

  // GET: /resources/exchanges/:symbol/markets
  async get_exchange_markets (symbol) {
    return await this.btr.Exchange.getMarketAll(symbol);
  }

  // GET: /resources/exchanges/:symbol/coins
  async get_exchange_coins (symbol) {
    return await this.btr.Exchange.getCoinAll(symbol);
  }

  // GET: /resources/markets
  async get_markets () {
    return await this.btr.Exchange.getMarketAll();
  }

  /*
   * GET: /resources/markets/:id
   * GET: /resources/markets/:symbol
   */
  get_market (type, value) {
  }

  // GET: /resources/coins
  async get_coins () {
    return await this.btr.Exchange.getCoinAll();
  }

  /*
   * GET: /resources/coins/:id
   * GET: /resources/coins/:symbol
   */
  get_coin (type, value) {
  }
}

module.exports = Resources;

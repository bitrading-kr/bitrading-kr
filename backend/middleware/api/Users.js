class Users {
  constructor (btrConnection) {
    this.btr = btrConnection;
  }

  // GET: /users
  get () {
  }

  // POST: /users
  post () {
  }

  // DELETE: /users
  delete () {
  }

  // GET: /users/:id
  get_id () {
  }

  // POST: /users/:id
  post_id () {
  }

  // DELETE: /users/:id
  delete_id () {
  }

}

module.exports = Users;

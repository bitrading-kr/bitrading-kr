const path = require('path')
module.exports = function Backend (_moduleOptions) {
  this.addPlugin(
    {
      src: path.resolve(__dirname, 'lib/backend.js'),
      fileName: 'backend.js'
    });
  this.addPlugin({
    src: path.resolve(__dirname, 'lib/plugin.template.js'),
    fileName: 'backend.template.js'
  });
}

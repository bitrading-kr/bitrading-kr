import DATA from './upbit.json';

let coins = {
  KRW: [],
  BTC: [],
  ETH: [],
  USDT: []
};

DATA.forEach(({market, korean_name})=> {
  let [_market, coin] = market.split('-');
  console.log(``);
  coins[_market].push({
    name: korean_name,
    symbol: coin
  });
});

export default {
  markets: [
    {
      name:"원화",symbol:"KRW"
    },
    {
      name:"비트코인",symbol:"BTC"
    },
    {
      name:"이더리움",symbol:"ETH"
    },
    {
      name:"테더",symbol:"USDT"
    }
  ],
  coins: coins
}

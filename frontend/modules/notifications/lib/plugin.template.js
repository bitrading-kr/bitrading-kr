import Vue from 'vue'
import VueNotifications from 'vue-notifications'
import 'izitoast/dist/css/iziToast.min.css'

export default function (ctx, inject) {
  if (process.browser) {
    const iziToast = require('izitoast');
    Vue.config.productionTip = false;

    function toast ({title, message, type, timeout, cb}) {
      if (type === VueNotifications.types.warn) type = 'warning'
      return iziToast[type]({title, message,
                             timeout: timeout,
                             progressBar: false});
    }

    const options = {
      success: toast,
      error: toast,
      info: toast,
      warn: toast
    };
    Vue.use(VueNotifications, options);
  }
}

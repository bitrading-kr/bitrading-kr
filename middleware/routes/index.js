var _ = require('lodash');
var url = require('url');
var express = require('express');
var router = express.Router();
var Backend = require('../middleware/backend.js');
var errorGenerator = require('../middleware/error-generator.js');
var BMM = require('bitrading-module-manager');
var axios = require('axios');
var crypto = require('crypto');

const validator = BMM.validator;
const error = BMM.error;
// const backend = new Backend();

function errorHandler (res, err) {
  let errcode = _.get(err, 'response.data.code');
  let parsedCode = errorGenerator.toJSON(errcode);
  return res.status(500).send(parsedCode).end();
}

function getUserAuthToken (req) {
  return _.get(req.cookies, 'authtoken') ||
    _.get(req, 'headers.auth-token');
}

const NODES = {
  auth: process.env.AUTH_NODE,
  order: process.env.ORDER_NODE
};

async function requestAxios (method, reqUrl, headerData, data) {
  let result;
  switch (method) {
  case 'get':
    result = await axios[method](reqUrl, {
      headers: headerData,
      params: data
    });
    break;
  case 'post':
  case 'put':
    result = await axios[method](reqUrl, data, {
      headers: headerData
    });
    break;
  default:
    break;
  }
  return result;
};

async function requestAuth (req, res, method, urlPath, data) {
  try {
    let result;
    let userAuthToken = getUserAuthToken(req);
    let headerData = {};
    if (userAuthToken != null && _.isString(userAuthToken)) {
      headerData = {
        'user-auth-token': userAuthToken
      };
    }

    let reqUrl = url.resolve(NODES.auth, urlPath);

    result = await requestAxios(method, reqUrl, headerData, data);
    return _.get(result, 'data');
  } catch (err) {
    errorHandler(res, err);
  }
}

async function requestOrder (req, res, method, urlPath, data) {
  try {
    let result;
    let userAuthToken = getUserAuthToken(req);
    let headerData = {};
    if (userAuthToken != null && _.isString(userAuthToken)) {
      headerData = {
        'user-auth-token': userAuthToken
      };
    }

    let reqUrl = url.resolve(NODES.order, urlPath);

    result = await requestAxios(method, reqUrl, headerData, data);
    return _.get(result, 'data');
  } catch (err) {
    errorHandler(res, err);
  }
}

function genOrderHeaders (req) {
  let token = _.get(req, 'headers.');
}
/** orders */
router.route('/order/orders')
  .get(async function (req, res, next) {
    let result = await requestOrder(req, res, 'get', 'orders');
    res.send(result).end();
  })
  .post(async function (req, res, next) {
    console.log(req.body.data);
    let result = await requestOrder(req, res, 'post', 'orders', req.body.data);
    res.send(result).end();
  });

router.route('/order/orders/:id')
  .get(async function (req, res, next) {
    let id = _.get(req, 'params.id');
    let result = await requestOrder(req, res, 'get', 'orders/' + id);
    res.send(result).end();
  })
  .delete(async function (req, res, next) {
    let id = _.get(req, 'params.id');
    let result = await requestOrder(req, res, 'delete', 'orders/' + id);
    res.send(result).end();
  });

/** apis */
router.get('/order/apis/:exchange', async function (req, res, next) {
  let exchange = _.get(req, 'params.exchange');
  let result = await requestOrder(req, res, 'get', 'apis/' + exchange);
  res.send(result).end();
});

router.put('/order/apis/:exchange', async function (req, res, next) {
  let exchange = _.get(req, 'params.exchange');
  let result = await requestOrder(req, res, 'put', 'apis/' + exchange, {
    access_key: req.body.accessKey,
    secret_key: req.body.secretKey
  });
  res.send(result).end();
});

/** trading */
router.get('/tradings', async function (req, res, next) {
  // let exchange = _.get(req, 'params.exchange');
  let query = req.query;
  console.log(query);
  let result = await requestOrder(req, res, 'get', 'tradings', query);
  res.send(result).end();
});

/** commands */
router.put('/commands/orders/:id', async function (req, res, next) {
  let command = _.get(req, 'body.command'),
      orderId = _.get(req, 'params.id');
  let result = await requestOrder(req, res, 'put', 'commands/orders/' + orderId, {
    command: command
  });
  res.send(result).end();
});

/** states */
router.get('/states', async function (req, res, next) {
  let result = await requestOrder(req, res, 'get', 'states');
  res.send(result).end();
});


/* Auth */
router.post('/auth/login', async function (req, res, next) {
  let formData = req.body,
      id = formData.id,
      pw = formData.pw;
  pw = crypto.createHash('sha512').update(pw, 'utf8').digest('base64');
  let result = await requestAuth(req, res, 'post', 'auth/login', {id: id, password: pw});
  res.send(result).end();
  return result;
});

router.get('/auth/login', async function (req, res, next) {
  console.log(req.headers);
  let result = await requestAuth(req, res, 'get', 'auth/login');
  res.send(result).end();
});

router.get('/auth/signup', function (req, res, next) {
  try {
    let formData = req.body,
        id = formData.id,
        pw = formData.pw;
    // requestBackend(res, 'post', 'auth/login', {id: id, password: pw});
  } catch (e) {
    errorHandler(res, e);
  }
});

module.exports = router;

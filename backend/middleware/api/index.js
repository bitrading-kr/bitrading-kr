require('dotenv').config();
const BitradingApi = require('@bitrading-api/bitrading');

const Users = require('./Users.js'),
      Groups = require('./Groups.js'),
      Orders = require('./Orders.js'),
      Resources = require('./Resources.js'),
      Auth = require('./Auth.js');

class Api {
  constructor (host, port, user, passwd, db) {
    this.btr = new BitradingApi(host, port, user, passwd, db);
  }

  /* Getters */
  get Users () {
    if (this._users == null) {
      this._users = new Users(this.btr);
    }
    return this._users;
  }

  get Groups () {
    if (this._groups == null) {
      this._groups = new Groups(this.btr);
    }
    return this._groups;
  }

  get Orders () {
    if (this._orders == null) {
      this._orders = new Orders(this.btr);
    }
    return this._orders;
  }

  get Resources () {
    if (this._resources == null) {
      this._resources = new Resources(this.btr);
    }
    return this._resources;
  }

  get Auth () {
    if (this._auth == null) {
      this._auth = new Auth(this.btr);
    }
    return this._auth;
  }
}

module.exports = Api;

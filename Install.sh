#!/bin/bash

sudo rm -rf ./middleware/node_modules
sudo rm -rf ./frontend/node_modules

sudo rm -f ./middleware/package-lock.json
sudo rm -f ./frontend/package-lock.json

sudo npm install --prefix ./middleware
sudo npm install --prefix ./frontend

docker-compose up --build -d
docker-compose logs -f

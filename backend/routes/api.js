const express = require('express'),
      router = express.Router(),
      _ = require('lodash');

// require middlewares
const Api = require('../middleware/api/index.js');
const api = new Api(process.env.DB_HOST, process.env.DB_PORT, process.env.DB_USER, process.env.DB_PASSWD, process.env.DB_DB);

function errorHandler (res, err) {
  return res.status(500).send(err).end();
}
/* GET home page. */
router.get('/', function(req, res, next) {
  res.send({"HI": "HEY"});
});

router.get('/login', function(req, res, next) {
  res.send({"status": "login"});
});

router.get('/resources', function(req, res, next) {
  res.send({"status": "login"});
});

/*
 * Auth
 */
router.post('/auth/login', function(req, res, next) {
  api.Auth.post_login(_.get(req, 'body.id'), _.get(req, 'body.password'))
    .then(result => res.send(result).end())
    .catch(err => errorHandler(res, err));
});

router.post('/auth/isLogin', function(req, res, next) {
  api.Auth.post_isLogin(_.get(req, 'body.token'))
    .then(result => res.send(result).end())
    .catch(err => errorHandler(res, err));
});

/*
 * Users
 */
router.post('/users/signin', function(req, res, next) {
  // console.log(req);
  res.send(req);
  // api.Resources.get_exchanges()
  //   .then(result => res.send(result).end())
  //   .catch(err => res.status(500).send(err));
});

/*
 * Exchanges
 */
router.get('/resources/exchanges', function(req, res, next) {
  api.Resources.get_exchanges()
    .then(result => res.send(result).end())
    .catch(err => errorHandler(res, err));
});

router.get('/resources/exchanges/:symbol', function(req, res, next) {
  api.Resources.get_exchange(req.params.symbol)
    .then(result => res.send(result).end())
    .catch(err => errorHandler(res, err));
});

router.get('/resources/exchanges/:symbol/markets', function(req, res, next) {
  api.Resources.get_exchange_markets(req.params.symbol)
    .then(result => res.send(result).end())
    .catch(err => errorHandler(res, err));
});

router.get('/resources/exchanges/:symbol/coins', function(req, res, next) {
  api.Resources.get_exchange_coins(req.params.symbol)
    .then(result => res.send(result).end())
    .catch(err => errorHandler(res, err));
});

router.get('/resources/markets', function(req, res, next) {
  api.Resources.get_markets()
    .then(result => res.send(result))
    .catch(err => errorHandler(res, err));
});

router.get('/resources/coins', function(req, res, next) {
  res.set('Content-Type', 'application/json');
  api.Resources.get_coins()
    .then(result => res.send(result))
    .catch(err => errorHandler(res, err));
});

module.exports = router;

// assert environment variables
require('bitrading-module-manager').assertEnv({
  MIDDLEWARE_PROTOCOL: 'protocol',
  MIDDLEWARE_HOST: 'host',
  MIDDLEWARE_PORT: 'port',
  MIDDLEWARE_ROOT: 'root'
})(
  process.env.MIDDLEWARE_PROTOCOL,
  process.env.MIDDLEWARE_HOST,
  process.env.MIDDLEWARE_PORT,
  process.env.MIDDLEWARE_ROOT
);

const fs = require('fs'),
      path = require('path'),
      url = require('url');

const BACKEND_API = `${process.env.MIDDLEWARE_PROTOCOL}://${process.env.MIDDLEWARE_HOST}:${process.env.MIDDLEWARE_PORT}${process.env.MIDDLEWARE_ROOT}`;
const BASE_URL = process.env.BASE_URL || 'http://localhost/';

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: '비트레이딩',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '비트레이딩 - 암호화폐 실시간 매매 시스템' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/images/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:400,500|Inconsolata:400,700|Roboto Mono:400' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css' }
    ],
    script:[
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    }
  },

  server: {
    // https: {
    //   key: fs.readFileSync(path.resolve(__dirname, 'cert/key.pem')),
    //   cert: fs.readFileSync(path.resolve(__dirname, 'cert/cert.pem'))
    // }
  },

  router: {
    middleware: ['init']
  },

  hooks: {
    ready(nuxt) {
    },
    build: {
      done (builder) {
      }
    },
    render: {
      routeContext(nuxt) {
      },
      done (renderer) {
      }
    },
  },

  modules: [
    ['@nuxtjs/axios', {
      proxyHeaders: false,
      baseURL: BASE_URL
    }],
    '@nuxtjs/proxy',
    ['@nuxtjs/google-analytics', {
      id: 'UA-128072422-1'
    }],
    '~/modules/errors',
    '~/modules/backend',
    '~/modules/notifications'
  ],

  plugins: [
    // { src: '~/plugins/axios.js' },
    { src: '~/plugins/cookies.js', ssr: true },
    // { src: '~/plugins/vue-analytics.js', ssr: true },
    { src: '~/plugins/vuejs-paginate.js', ssr: false },
    { src: '~/plugins/vue-sanitize.js', ssr: false },
    { src: '~/plugins/vue-sweetalert2.js', ssr: false }
  ],

  proxy: {
    '/api': { target: BACKEND_API, secure: false, crossorigin: true }
    // '/auth/login': { target: BACKEND_API, secure: false, crossorigin: true },
    // '/auth/isLogin': { target: BACKEND_API, secure: false, crossorigin: true },
    // '/auth/signup': { target: BACKEND_API, secure: false, crossorigin: true }
  },

  env: {
    baseUrl: BASE_URL,
    BACKEND_API: BACKEND_API,
    SUPPORT_EXCHANGES: ['UPBIT'],
    NODE_ENV: process.env.NODE_ENV
  }
}

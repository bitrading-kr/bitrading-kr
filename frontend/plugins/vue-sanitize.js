import Vue from 'vue';
import VueSanitize from "vue-sanitize";

const defaultOptions = {
  allowedTags: ['b', 'i'],
  allowedAttributes: {
  }
};
Vue.use(VueSanitize, defaultOptions);

import UPBIT from './exchange-data/upbit.js';

var EXCAHNGES = [
  {
    name: '업비트',
    symbol: 'UPBIT'
  }
];
const MARKETS = {
  UPBIT: UPBIT.markets
};
const COINS = {
  UPBIT: UPBIT.coins
};

export default {
  state: {
    req: 0,
    exchange: EXCAHNGES,
    coin: COINS,
    market: MARKETS,
  }
}

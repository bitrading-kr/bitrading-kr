const { validator, error } = require('bitrading-module-manager');

class Auth {
  constructor (btrConnection) {
    this.btr = btrConnection;
  }

  // POST: /auth/signin
  async post_signin (id, pw, email) {
    // return await this.btr.User.register(id, pw, email);
  }

  // POST: /auth/login
  async post_login (id, pw) {
    try {
      try {
        id = validator('auth/id', id);
        pw = validator('auth/pw', pw);
      } catch (e) {
        console.log(e);
        throw error.new(error.codes.AUTH.LOGIN.PARAM.INVALID);
      }
      try {
        return await this.btr.Auth.login(id, pw);
      } catch (e) {
        throw error.new(error.codes.AUTH.LOGIN.FAILED.NOMATCH);
      }
    } catch (e) {
      throw e;
    }
  }

  async post_isLogin (token) {
    try {
      return await this.btr.Auth.isLogin(token);
    } catch (e) {
      throw e;
    }
  }
}

module.exports = Auth;

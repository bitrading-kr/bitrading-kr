const path = require('path');

import Vuex from 'vuex'
import Resources from './resources.js'

export default () => {
  return new Vuex.Store({
    modules: {
      Resources
    },
    state: {
      location: {
        'image': '/images',
        'icon': '/images/icons',
        'coin': '/images/icons/coins',
        'market': '/images/icons/markets',
        'exchange': '/images/icons/exchanges',
      },
      site: {
        title: '비트레이딩',
        description: '암호화폐 트레이더 연동 거래소 매매 시스템',
        logo: '/images/logo.png',
        icon: '/images/icon.png'
      },
      page: {
        title: ''
      },
      user: {
        isLogin: false,
        name: '',
        email: ''
      }
    },
    getters: {
      getTitle: state => {
        return state.page.title + ' - ' + state.site.title;
      },
      initUser: state => {
        state.user.name = "홍길동";
        state.user.email = "hong@gmail.com";
      },
      resolveLoc: (state, key, loc) => {
        return path.resolve(state.location[key], loc);
      },
      checkLogin: (state) => {
        state.user.isLogin = true;
      }
    }
  });
}

export default function ({ $axios, env, redirect }) {
  $axios.defaults.baseURL = env.baseUrl;
  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      redirect('/400')
    }
  });
}

import _ from 'lodash'
import mergeOptions from 'merge-options'
import qs from 'querystring'

const ERR_CODE = {
  ACCESS: { name: '접근권한이 없습니다.',
            redirect: '/errors/access' },
  LOGIN: { name: '로그인이 필요합니다.',
           redirect: '/errors/login' }
};

class Errors {
  throw (type, message, options={}) {
    let params = mergeOptions(ERR_CODE[type], {
      message: message
    }, options);
    this.redirect({path: params.redirect, query: params});
  }
  access (message) {
    this.throw('ACCESS', message);
  }

  login (message) {
    this.throw('LOGIN', message);
  }

  set router (router) {
    this._router = router;
  }
  get router () {
    return this._router;
  }

  set redirect (redirect) {
    this._redirect = redirect;
  }
  get redirect () {
    return this._redirect;
  }
}

export default new Errors();

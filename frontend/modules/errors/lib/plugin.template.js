import errors from './errors'

export default function (ctx, inject) {
  errors.router = ctx.app.router;
  errors.redirect = ctx.redirect;
  ctx.$errors = errors;
  inject('errors', errors);
}

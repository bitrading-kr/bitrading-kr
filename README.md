# Bitrading

비트레이딩 - 암호화폐 트레이더 오더 기반 매매 시스템

## 문서

### Roadmap

[Roadmap.md](https://gitlab.com/bitrading-kr/roadmap)


### 시연

[Demo.md](./docs/Demo.md)


## Versions

### 0.2.2(2018-11-17)
- trading 테이블 뷰 추가
- 레이아웃 수정

### 0.2.1(2018-11-13)
- 베타테스팅

### 0.1.0(2018-10-30)

- 초기화

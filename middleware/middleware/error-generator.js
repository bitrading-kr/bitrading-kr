var { error } = require('bitrading-module-manager'),
    _ = require('lodash');

const errorTemplate = {
  AUTH: {
    LOGIN: {
      PARAM: {
        INVALID: '아이디 혹은 비밀번호 형식이 맞지 않습니다.'
      },
      FAILED: '아이디 혹은 비밀번호를 확인하세요.'
    }
  }
};

class ErrorGenerator {
  constructor () {
    this.keyList = ['namespace', 'result', 'status', 'reason'];
  }

  gen (errCode) {
    const parsed = error.parseCode(errCode);

    let codeList = _.map(parsed, elem => {
      return _.get(elem, 'name');
    }).filter(elem => elem != null);

    let customMsg = _.get(errorTemplate, codeList);
    if (codeList.length > 0 && _.isString(customMsg)) {
      _.set(parsed, [this.keyList[codeList.length-1], 'message'], customMsg);
    }

    return parsed;
  }

  toJSON (errCode) {
    let data = this.gen(errCode);
    let title = '', msg = '';

    // set message
    this.keyList.forEach(key => {
      let message = _.get(data, [key, 'message']);
      if (_.isString(message)) {
        msg = message;
      }
    });

    this.keyList.slice(0, 2).forEach(key => {
      let message = _.get(data, [key, 'message']);
      if (_.isString(message)) {
        title = message;
      }
    });

    return {
      title: title,
      message: msg
    };
  }
}

module.exports = new ErrorGenerator();

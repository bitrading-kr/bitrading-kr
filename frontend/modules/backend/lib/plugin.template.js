import backend from './backend'

export default function (ctx, inject) {
  const { BACKEND_API } = ctx.env;
  backend.host = ctx.env.BACKEND_API;
  ctx.$backend = backend;
  inject('backend', backend);
}

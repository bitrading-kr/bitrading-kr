const axios = require('axios'),
      url = require('url');

class Backend {
  constructor (protocol=process.env.BACKEND_PROTOCOL, host=process.env.BACKEND_HOST, port=process.env.BACKEND_PORT, root=process.env.BACKEND_ROOT) {
    if (protocol == null) throw new Error('BACKEND_PROTOCOL is not set');
    if (host == null) throw new Error('BACKEND_HOST is not set');
    if (port == null) throw new Error('BACKEND_PORT is not set');
    if (root == null) throw new Error('BACKEND_ROOT is not set');
    this.host = `${protocol}://${host}:${port}${root}`;
  }

  resolve (_path) {
    return url.resolve(this.host, _path);
  }

  get (_path, params={}) {
    return new Promise((resolve, reject) => {
      try {
        let host = this.resolve(_path);
        console.log(params);
        axios.get(host, params)
          .then(res => resolve(res))
          .catch(err => reject(err));
      } catch (e) {
        reject(e);
      }
    });
  }

  post (_path, params={}) {
    return new Promise((resolve, reject) => {
      try {
        let host = this.resolve(_path);
        console.log(host);
        axios.post(host, params)
          .then(res => resolve(res))
          .catch(err => reject(err));
      } catch (e) {
        reject(e);
      }
    });
  }
}

module.exports = Backend;
